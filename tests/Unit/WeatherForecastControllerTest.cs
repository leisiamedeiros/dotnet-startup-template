using Microsoft.Extensions.Logging;
using Moq;
using Startup.Template.WebApi;
using Startup.Template.WebApi.Controllers;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Startup.Template.UnitTests;

public class WeatherForecastControllerTest
{
    [Fact]
    public void Get_WeatherForecast_Success()
    {
        //Arrange
        var mockLogger = new Mock<ILogger<WeatherForecastController>>();

        var controller = new WeatherForecastController(mockLogger.Object);

        // Act
        var result = controller.Get();

        // Assert
        Assert.IsAssignableFrom<IEnumerable<WeatherForecast>>(result);

        Assert.Equal(5, result.Count());
    }
}