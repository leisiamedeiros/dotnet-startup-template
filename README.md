# Dotnet Startup Template

[![pipeline status](https://gitlab.com/leisiamedeiros/dotnet-startup-template/badges/developer/pipeline.svg)](https://gitlab.com/leisiamedeiros/dotnet-startup-template/-/commits/developer)

`dotnet-startup-template` is a repository made with an ASP.NET API template that has the objective to show you how CI/CD works with GitLab.

Here you can see how to configure the CI/CD of an ASP.NET Web api with docker image in gitlab registry, as well as a package. You will see too a pipeline with test and SAST to analyze your application source code.

## Installation

After clone this repository, in the terminal navigate for the WebApi folder to build and run this project

```bash
$ dotnet build
$ dotnet run
```

## Usage

Endpoints:
- [Swagger Page](http://localhost:5000/swagger/index.html)
- [Api Weatherforecast](http://localhost:5000/api/weatherforecast)

## WebApi docker image

The image of this WebApi application is stored using Gitlab [Container Registry](https://gitlab.com/leisiamedeiros/dotnet-startup-template/container_registry) and can be used with the `docker-compose.yml` file through the command `docker-compose up -d` or manually with the command:

```bash
docker run -d -p 5000:80 --name startupwebapi registry.gitlab.com/leisiamedeiros/dotnet-startup-template:latest
```
After the execution command you can check if the container is running with the command

```bash
docker ps
```

If the startupwebapi service is running you can access the [application](http://localhost:5000/swagger/index.html).

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.